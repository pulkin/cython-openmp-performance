#!/usr/bin/env python3
import numpy as np
import test
import time

large_array = np.arange(50000000, dtype=float)
start = time.time()
test.test_function(large_array)
print(time.time() - start, large_array[:5])

