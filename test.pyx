# cython: language_level=2
from cython import boundscheck, wraparound
from cython.parallel import prange, threadid
from libc.math cimport exp, cos
from libc.stdio cimport printf

@boundscheck(False)
@wraparound(False)
def test_function(double[::1] data):
    cdef int index, thread_id
    cdef int size = data.shape[0]

    for index in prange(16, nogil=True, schedule='static'):
        thread_id = threadid()
        printf("%d\n", thread_id)

    for index in prange(size, nogil=True, schedule='static'):
        data[index] = exp(cos(data[index] ** 2) + 3)

