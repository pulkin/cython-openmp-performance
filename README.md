cython-openmp-performance
-------------------------

Minimal working example where `cython` fails to prepare an `openmp`-parallel setup.

```bash
setup.py build --force
ln -s build/lib.replace-this/test.and-this.so test.so
OMP_NUM_THREADS=4 python benchmark.py
```

